package com.codete.jjhorbacz.collection;

public class Element{

    private String value;
    private Element previous;
    private Element next;

    public Element(String value) {
        this.value = value;
    }

    public Element getNext() {
        return next;
    }

    public Element getPrevious() {
        return previous;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public void setPrevious(Element previous) {
        this.previous = previous;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}