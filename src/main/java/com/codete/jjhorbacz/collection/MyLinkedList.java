package com.codete.jjhorbacz.collection;

//its a program where i create my own linked list
//firstElement is always first element of the list
//lastElement is always last element of the list
//currentElement is element behind lastElement
//add method include
//addWithIndex method include
//showList method include
//setCompanyNameWithIndex method included

public class MyLinkedList {

    Element firstElement;
    Element lastElement;
    Element currentElement;
    Element nextElement;

    public static void main(String[] args) {

        MyLinkedList list = new MyLinkedList();

        list.add("Codete");
        list.add("Britenet");
        list.add("Sii");
        list.add("MicroBit");
        list.add("Google");
        list.add("Microsoft");
        list.add("Apple");
        list.add("COMTRONIC");
        list.add("DataArt");
        list.add("MADIFF");
        list.add("IMPAQ");
        System.out.println("Companies list: ");
        list.showList();
        System.out.println();
        System.out.println();
        System.out.println("First Element: "+list.getFirstElement());
        System.out.println("Last Element: "+list.getLastElement());
        System.out.println();
        list.setCompanyNameWithIndex("Microsoft2",5);
        System.out.println("/change 'Microsoft' to 'Microsoft2':");
        list.showList();
        System.out.println();
        list.addWithIndex2("ADD:2", 2);
        System.out.println();
        System.out.println("Add sth with index 2:");
        list.showList();

    }

    public void add(String companyName) {
        if (firstElement == null) {
            firstElement = new Element(companyName);
            lastElement = firstElement;
        } else {
            Element currentElement = firstElement;
            while (currentElement.getNext() != null) {
                currentElement = currentElement.getNext();
            }
            lastElement = new Element(companyName);
            lastElement.setPrevious(currentElement);
            currentElement.setNext(lastElement);
        }
    }

    public void setCompanyNameWithIndex(String companyName, int index) {
        nextElement = firstElement;
        for (int i=0;i<index;i++){
            nextElement = nextElement.getNext();
        }
        nextElement.setValue(companyName);
    }

    public void addWithIndex2(String companyName, int index) {
        currentElement = firstElement;
        while (currentElement.getNext() != null) {
            currentElement = currentElement.getNext();
        }
        lastElement = new Element(companyName);
        lastElement.setPrevious(currentElement);
        currentElement.setNext(lastElement);
        nextElement = firstElement;
        for(int i=0;i<index;i++){
            nextElement = nextElement.getNext();
        }
        while(lastElement.getValue() != nextElement.getValue()){
            lastElement.setValue(lastElement.getPrevious().getValue());
            lastElement = lastElement.getPrevious();
        }
        nextElement.setValue(companyName);
    }

    public void addWithIndex(String companyName, int index){
        nextElement = firstElement;
        for(int i=0;i<index;i++){
            nextElement = nextElement.getNext();
        }
        nextElement.setNext(nextElement);
        nextElement = new Element(companyName);
    }

    public void showList() {
        nextElement = firstElement;
        while (nextElement != null) {
            System.out.print(nextElement + "; ");
            nextElement = nextElement.getNext();
        }

    }

    public Element getFirstElement() {
        return firstElement;
    }

    public Element getLastElement() {
        return lastElement;
    }

}
